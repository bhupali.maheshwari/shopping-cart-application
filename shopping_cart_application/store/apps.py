from django.apps import AppConfig


class ShopusersConfig(AppConfig):
    name = 'store'
