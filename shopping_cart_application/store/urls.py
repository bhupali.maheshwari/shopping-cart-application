from django.urls import path, include
from django.views.generic import TemplateView

from .views import display_store, display_cart, display_orders, checkout_view, update_cart, delete_order


urlpatterns = [
    path('', display_store, name='display-store'),
    path('cart/<int:pk>', display_cart, name='display-cart'),
    path('checkout/<int:pk>', checkout_view, name='checkout'),
    path('update_cart/', update_cart, name='update-cart'),
    path('order_place/', TemplateView.as_view(template_name='store/order_placed.html'), name='order-placed'),
    path('orders/', display_orders, name='display-orders'),
    path('delete_order/', delete_order, name='delete-order'),
]