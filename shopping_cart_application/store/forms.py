from django import forms

from allauth.account.forms import SignupForm
from .models import ShippingDetails


class CustomSignUpForm(SignupForm):
    TRUE_FALSE_OPTIONS = (
        (1, "Yes"),
        (0, "No"),
    )
    first_name = forms.CharField(max_length=30, label='First Name')
    last_name = forms.CharField(max_length=30, label='Last Name')
    is_shop_user = forms.TypedChoiceField(choices = TRUE_FALSE_OPTIONS, label='Are you a Shop User?', widget=forms.RadioSelect, coerce=int)

    def custom_signup(self, request, user):
        user.first_name = self.cleaned_data.get('first_name')
        user.last_name = self.cleaned_data.get('last_name')
        is_shop_user = self.cleaned_data.get('is_shop_user', False)
        if is_shop_user:
            user.is_shop_user = True
        else:
            user.is_customer = True
        user.save()


class ShippingForm(forms.ModelForm):
    class Meta:
        model = ShippingDetails
        fields = ('address', 'city', 'state', 'zipcode')
        widgets = {
            'address': forms.TextInput(attrs={'size': 50}),
        }