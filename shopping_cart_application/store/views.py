import uuid
from datetime import datetime
import json

from django.shortcuts import render, redirect
from django.views.generic import ListView, DetailView, View, FormView, CreateView, DeleteView, UpdateView
from django.http import JsonResponse
from django.urls import reverse_lazy, reverse

from .models import Product, Category, Cart, User, CartItem, ShippingDetails
from .forms import ShippingForm


class DisplayStore(ListView):
    template_name = 'store/product.html'

    def get_queryset(self):
        category_id = self.request.GET.get('category')
        search_query = self.request.GET.get('search-query')
        if category_id:
            product_list = Product.objects.filter(category_of_product=category_id)
        elif search_query:
            product_list = Product.objects.filter(product_name__icontains=search_query)
        else:
            product_list = Product.objects.all()
        return product_list

    def get_context_data(self, **kwargs):
        context = super(DisplayStore, self).get_context_data()
        if self.request.user.is_authenticated:
            cart, created = Cart.objects.get_or_create(customer=self.request.user, active=True)
            context['cart'] = cart
            context['total_cart_items'] = cart.get_total_items
        context['categories'] = Category.objects.all()
        return context


class DisplayCart(DetailView):
    template_name = 'store/cart.html'
    model = Cart

    def get_context_data(self, **kwargs):
        context = super(DisplayCart, self).get_context_data()
        self.object = self.get_object()
        context['cart_items'] = CartItem.objects.filter(cart = self.object)
        context['cart'] = self.object
        return context


class Checkout(DisplayCart, CreateView):
    template_name = 'store/checkout.html'
    form_class = ShippingForm
    success_url = reverse_lazy('order-placed')

    def form_valid(self, form):
        form.instance.customer = self.request.user
        cart = Cart.objects.get(customer=form.instance.customer, active=True)
        cart.active = False
        cart.order_date = datetime.now()
        cart.transaction_id = uuid.uuid1()
        cart.cart_status = 'order_placed'
        cart.save()

        cart_items = CartItem.objects.filter(cart=cart)
        for item in cart_items:
            product = Product.objects.get(id=item.product.id)
            product.quantity -= item.product_no
            product.save()
        form.instance.cart = cart
        return super(Checkout, self).form_valid(form)


class UpdateCart(View):
    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)
        product_id = data.get('product_id')
        action = data.get('action')

        product = Product.objects.get(id=product_id)
        cart, created = Cart.objects.get_or_create(customer = request.user, active=True)
        item, created = CartItem.objects.get_or_create(cart=cart, product=product)

        if action == "add":
            item.product_no += 1
        elif action == "remove":
            item.product_no -= 1
        item.save()

        if item.product_no <= 0:
            item.delete()
        return JsonResponse("Cart updated successfully", safe=False)


class DisplayOrders(DisplayStore):
    template_name = 'store/orders.html'

    def get_context_data(self, **kwargs):
        context = super(DisplayOrders, self).get_context_data()
        past_orders = Cart.objects.filter(customer = self.request.user, active=False)
        for order in past_orders:
            shipping_details = ShippingDetails.objects.get(cart=order)
            cart_items = CartItem.objects.filter(cart=order)
            order.shipping_details = shipping_details
            order.cart_items = cart_items
        context['past_orders'] = past_orders
        return context


class DeleteOrder(View):

    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)
        order_id = data.get('order_id')
        cart = Cart.objects.get(id=order_id)
        cart.cart_status = 'cancelled'
        cart.save()

        cart_items = CartItem.objects.filter(cart=cart)
        for item in cart_items:
            product = Product.objects.get(id=item.product.id)
            product.quantity += item.product_no
            product.save()
        return JsonResponse("Order deleted successfully", safe=False)


display_store = DisplayStore.as_view()
display_cart = DisplayCart.as_view()
checkout_view = Checkout.as_view()
update_cart = UpdateCart.as_view()
display_orders = DisplayOrders.as_view()
delete_order = DeleteOrder.as_view()