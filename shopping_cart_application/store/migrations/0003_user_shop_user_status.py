# Generated by Django 3.1.7 on 2021-04-01 07:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0002_product_publish_item'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='shop_user_status',
            field=models.CharField(choices=[('approved', 'Approved'), ('rejected', 'Rejected')], default='pending', max_length=50),
        ),
    ]
