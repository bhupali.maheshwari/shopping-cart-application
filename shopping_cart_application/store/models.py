from datetime import datetime

from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    STATUS_CHOICES = (
        ('approved', 'Approved'),
        ('rejected', 'Rejected'),
        ('pending', 'Pending'),
    )
    is_customer = models.BooleanField(default=False)
    is_shop_user = models.BooleanField(default=False)
    shop_user_status = models.CharField(max_length=50, choices=STATUS_CHOICES, default='pending')

    def __str__(self):
        return self.username


class Category(models.Model):
    category_name = models.CharField(max_length=100)

    def __str__(self):
        return self.category_name


class Product(models.Model):
    product_name = models.CharField(max_length=100)
    price = models.FloatField()
    quantity = models.IntegerField(default=0)
    category_of_product = models.ManyToManyField(Category)
    details = models.TextField()
    publish_date = models.DateField(default=datetime.now)
    publish_item = models.BooleanField(default=True)
    image = models.ImageField(null=True, blank=True)

    def __str__(self):
        return self.product_name

    @property
    def imageURL(self):
        try:
            url = self.image.url
        except:
            url = ''
        return url


class Cart(models.Model):
    STATUS_CHOICES = (
        ('in_progress', 'In Progress'),
        ('order_placed', 'Order Placed'),
        ('confirmed', 'Confirmed'),
        ('cancelled', 'Cancelled'),
        ('out_for_delivery', 'Out for Delivery'),
        ('delivered', 'Delivered'),
        ('cancelled', 'Order Cancelled')
    )
    customer = models.ForeignKey(User, on_delete=models.CASCADE)
    active = models.BooleanField(default=True)
    transaction_id = models.UUIDField(default=0)
    order_date = models.DateTimeField(default=datetime.now)
    cart_status = models.CharField(max_length=50, choices=STATUS_CHOICES, default='in_progress')

    def __str__(self):
        return str(self.id)

    @property
    def get_total_items(self):
        cartitems = self.cartitem_set.all()
        total = 0
        for item in cartitems:
            total += item.product_no
        return total

    @property
    def get_total_amount(self):
        cartitems = self.cartitem_set.all()
        total = 0
        for item in cartitems:
            total += item.get_product_total
        return total


class CartItem(models.Model):
    """
    This represents items in the cart.
    """
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    cart = models.ForeignKey(Cart, on_delete=models.CASCADE)
    product_no = models.IntegerField(default=0)
    date_added = models.DateTimeField(default=datetime.now)

    @property
    def get_product_total(self):
        return self.product_no * self.product.price


class ShippingDetails(models.Model):
    customer = models.ForeignKey(User, on_delete=models.CASCADE)
    cart = models.ForeignKey(Cart, on_delete=models.CASCADE)
    address = models.TextField()
    city = models.CharField(max_length=50)
    state = models.CharField(max_length=100)
    zipcode = models.CharField(max_length=10)

    def __str__(self):
        return self.address
