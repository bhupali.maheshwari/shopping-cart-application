from django import forms

from store.models import User


class DisplayRequest(forms.Form):
    STATUS_CHOICES = (
        ('approved', 'Approve'),
        ('rejected', 'Reject'),
    )
    users = forms.ChoiceField()
    status = forms.ChoiceField(choices=STATUS_CHOICES)

    def __init__(self, *args, **kwargs):
        super(DisplayRequest, self).__init__(*args, **kwargs)
        list_of_users = []
        user_data = User.objects.filter(shop_user_status="pending", is_shop_user=True)
        for user in user_data:
            list_of_users.append((user.username, user.first_name + ' ' + user.last_name))
        self.fields['users'].choices = list_of_users