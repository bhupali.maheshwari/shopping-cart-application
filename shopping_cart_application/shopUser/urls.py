from django.urls import path, include

from .views import edit_product, add_product, delete_product, add_category, show_requests, shop_user_dashboard, update_status, view_order, covert_to_customer_account


urlpatterns = [
    path('update_product/<int:pk>', edit_product, name='edit-product'),
    path('delete_product/<int:pk>', delete_product, name='delete-product'),
    path('add_product/', add_product, name='add-product'),
    path('add_category/', add_category, name='add-category'),
    path('dashboard/', shop_user_dashboard, name='shopuser-dashboard'),
    path('update_status/<int:pk>', update_status, name='update-status'),
    path('view_order/<int:pk>', view_order, name='view-order'),
    path('show_requests/', show_requests, name='display-request'),
    path('covert_account/', covert_to_customer_account, name='covert-account-to-customer'),
]