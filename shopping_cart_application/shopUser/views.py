from django.shortcuts import render, redirect
from django.views.generic import ListView, DetailView, View, FormView, CreateView, DeleteView, UpdateView
from django.urls import reverse_lazy

from store.models import Product, Category, Cart, User, CartItem, ShippingDetails
from .forms import DisplayRequest


class EditProduct(UpdateView):
    model = Product
    fields = [
        'product_name', 'price', 'quantity', 'category_of_product', 'details', 'publish_item', 'image'
    ]
    success_url = reverse_lazy('display-store')
    template_name = "shopUser/update_product.html"


class DeleteProduct(DeleteView):
    model = Product
    success_url = reverse_lazy('display-store')
    template_name = "shopUser/delete_product.html"


class AddProduct(CreateView):
    model = Product
    fields = [
        'product_name', 'price', 'quantity', 'category_of_product', 'details', 'publish_item', 'image'
    ]
    success_url = reverse_lazy('display-store')
    template_name = "shopUser/add_product.html"


class AddCategory(CreateView):
    model = Category
    fields = [
        'category_name',
    ]
    success_url = reverse_lazy('display-store')
    template_name = "shopUser/add_category.html"


class ShopuserDashboard(ListView):
    template_name = "shopUser/shopuser_dashboard.html"
    context_object_name = "cart"

    def get_queryset(self):
        cart = Cart.objects.filter(active=False)
        return cart

    def get_context_data(self, **kwargs):
        context = super(ShopuserDashboard, self).get_context_data()
        context['total_orders'] = Cart.objects.filter(active=False).count
        context['total_delivered'] = Cart.objects.filter(active=False, cart_status='delivered').count
        context['total_pending'] = Cart.objects.exclude(cart_status='delivered').exclude(active=True).count
        return context


class UpdateStatus(UpdateView):
    model = Cart
    fields = [
        'cart_status',
    ]
    success_url = reverse_lazy('shopuser-dashboard')
    template_name = "shopUser/update_status.html"


class ViewOrder(DetailView):
    template_name = 'shopUser/view_cart.html'
    model = Cart
    context_object_name = "order"

    def get_context_data(self, **kwargs):
        context = super(ViewOrder, self).get_context_data()
        self.object = self.get_object()
        context['cart_items'] = CartItem.objects.filter(cart=self.object)
        context['shipping_details'] = ShippingDetails.objects.get(cart=self.object)
        return context


class ShowRequests(FormView):
    form_class = DisplayRequest
    template_name = 'shopUser/show_requests.html'
    success_url = reverse_lazy('admin:index')

    def form_valid(self, form):
        user = form.cleaned_data['users']
        user_status = form.cleaned_data['status']
        update_user = User.objects.get(username=user)
        update_user.shop_user_status = user_status
        update_user.save()
        return super(ShowRequests, self).form_valid(form)


class CovertToCustomerAccount(View):
    def get(self, request, *args, **kwargs):
        user = User.objects.get(username=request.user.username)
        user.is_customer = True
        user.is_shop_user = False
        user.shop_user_status = "pending"
        user.save()
        return redirect('display-store')


edit_product = EditProduct.as_view()
delete_product = DeleteProduct.as_view()
add_product = AddProduct.as_view()
add_category = AddCategory.as_view()
shop_user_dashboard = ShopuserDashboard.as_view()
update_status= UpdateStatus.as_view()
view_order = ViewOrder.as_view()
show_requests = ShowRequests.as_view()
covert_to_customer_account = CovertToCustomerAccount.as_view()