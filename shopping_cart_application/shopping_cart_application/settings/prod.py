import dj_database_url

from .base import *

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ.get('SECRET_KEY', 'gb#6s3s)8s%k1bl6a*0dxk^m(klfml)1dti!1ljfpm*u(0pn1-')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = ['localhost', 'shopping-cart-docker.herokuapp.com']

EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
EMAIL_HOST = "smtp.gmail.com"
EMAIL_HOST_USER = "*****"
EMAIL_HOST_PASSWORD = "******"
EMAIL_PORT = 587
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = "shoppingapp@gmail.com"

# DB settings for Heroku
DATABASE_URL = os.environ.get('DATABASE_URL')
db_from_env = dj_database_url.config(default=DATABASE_URL, conn_max_age=500, ssl_require=True)
DATABASES = {
    'default': db_from_env
}

AWS_ACCESS_KEY_ID = 'AKIAXIZBT52QAHB62RNJ'
AWS_SECRET_ACCESS_KEY = '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'
AWS_STORAGE_BUCKET_NAME = 'media-storage-bucket1'

AWS_S3_FILE_OVERWRITE = False
AWS_DEFAULT_ACL = None
DEFAULT_FILE_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'   # To upload media files to s3
# STATICFILES_STORAGE = 'storages.backends.s3boto3.S3StaticStorage'