var updateCart = document.getElementsByClassName('update-cart')
var deleteOrder = document.getElementsByClassName('delete-order')

for (i = 0; i < deleteOrder.length; i++) {
    deleteOrder[i].addEventListener('click', function() {
        var orderID = this.dataset.order
        deleteShippingDetails(orderID)
    })
}

for (i = 0; i < updateCart.length; i++) {
    updateCart[i].addEventListener('click', function() {
        var productID = this.dataset.product
        var action = this.dataset.action
        if(user === "AnonymousUser") {
            console.log("User not logged in. Please login to add to cart.")
            window.location.href = window.location.origin + '/accounts/login/'
        }
        else {
            console.log("User logged in.")
            updateUserCart(productID, action)
        }
    })
}

function updateUserCart(productID, action) {
    console.log("Updating cart: action=", action)
    var url = window.location.origin + '/store/update_cart/'
    fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'X-CSRFToken': csrftoken,
        },
        body: JSON.stringify({'product_id': productID, 'action': action})
    })
    .then((response) => {
        return response.json()
    })
    .then((data) => {
        console.log(data)
        window.location.reload();
    })
}

function deleteShippingDetails(orderID) {
    console.log("Deleting Order")
    var url = window.location.origin + '/store/delete_order/'
    fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'X-CSRFToken': csrftoken,
        },
        body: JSON.stringify({'order_id': orderID})
    })
    .then((response) => {
        return response.json()
    })
    .then((data) => {
        console.log(data)
        window.location.reload();
    })
}