FROM python:3

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN mkdir /webCode
WORKDIR /webCode

COPY requirements.txt /webCode/
RUN pip install -r requirements.txt
COPY . /webCode/

WORKDIR /webCode/shopping_cart_application

RUN python manage.py collectstatic --noinput
CMD gunicorn shopping_cart_application.wsgi:application --bind 0.0.0.0:$PORT