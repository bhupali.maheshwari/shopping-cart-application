To do local setup:

Clone the project and run below command:
> docker-compose build

> docker-compose up

You can also access the application via below url:
https://shopping-cart-docker.herokuapp.com/store/
